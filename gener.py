import re
import json
import urllib.parse
import requests
from pywebio import start_server
from pywebio.output import *
from pywebio.input import input


def put_song_info(resource):
    put_table([['封面','标题','P主',],
           [put_image(resource[0]),
            put_html(str(resource[1])
                     .replace('href="','target="_blank" href="https://zh.moegirl.org.cn')),
            put_html(str(resource[2])
                     .replace('href="','target="_blank" href="https://zh.moegirl.org.cn'))]])

def add_lib(listhash,list0):
    put_text('萌百链接格式：https://zh.moegirl.org.cn/VOCALOID传说曲\nor\nhttps://zh.moegirl.org.cn/VOCALOID%E4%BC%A0%E8%AF%B4%E6%9B%B2')
    url = input("输入萌百链接")
    res = urllib.parse.unquote(str(requests.get(url).text)).replace('&#58;', ':')
    tmp = re.compile(r'<div class="famed-song-imgbox"><img.*?src="(.*?)".*?></div>.*?<b>曲目</b>：(.*?)</div>.*?<b>P主</b>：(.*?)</div>').findall(res)
    put_collapse('原始数据:共{}条'.format(len(tmp)), put_table(tmp))
    toast("读取成功(大概率吧？)", color='success')
    put_text('输出示例：')
    put_song_info(tmp[0])

    #整合输出模块
    toast("尝试整合输出")
    put_text("原始数据：{}".format(len(list0)))
    # put_text(listhash[0])
    # put_text(hash(tmp[0][0]))
    # put_text(list0[0][0])
    # put_text((tmp[0][0]))
    for i in tmp:
        if not i[0] in listhash:
            list0.append(i)
    put_text("新数据：{} 追加后数据：{}".format(len(tmp), len(list0)))

    with open('./resource.json', 'w') as f:
        f.write(json.dumps(list0))
        toast("输出成功(大概率吧？)", color='success')

    #输出哈希表
    listhash = []
    for i in list0:
        listhash.append(i[0])
    with open('./resource(hash).json', 'w') as f:
        f.write(json.dumps(listhash))
        toast("哈希表输出成功(大概率吧？)", color='success')

def main():
    put_info("LOADING...")
    list0 = []
    listhash = []
    try:
        list0 = list(json.load(open('./resource.json')))
        listhash = list(json.load(open('./resource(hash).json')))
        toast("已读取原始数据：{}".format(len(list0)), color='success')
    except:
        toast("读取本地失败", color='error')
    add_lib(listhash, list0)

if __name__ == "__main__":
    #main()
    start_server(main, port=8081)